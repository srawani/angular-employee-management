import { DepartmentTableComponent } from './department-table/department-table.component';
import { EmployeeTableComponent } from './employee-table/employee-table.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'employee', component: EmployeeTableComponent },
  { path: 'department',      component: DepartmentTableComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: true } // <-- debugging purposes only
    )
    // other imports here
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {


 }



