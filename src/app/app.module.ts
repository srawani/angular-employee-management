import { DepartmentService } from './department-table/department-service';
import { EmployeeService } from './employee-table/employee-service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeTableComponent } from './employee-table/employee-table.component';
import { DepartmentTableComponent } from './department-table/department-table.component';
import { HttpClient,HttpHandler } from '@angular/common/http';
import { HttpClientModule } from "@angular/common/http";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    EmployeeTableComponent,
    DepartmentTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [EmployeeService, DepartmentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
