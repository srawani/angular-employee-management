
import {Department} from './department';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})

export class DepartmentService {

  private api = 'http://statefarm-department-management.cfapps.io/api/department';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  getAllData(): Observable<Department[]> {
    return this.httpClient.get<Department[]>(this.api);
  }
  getData(id: number): Observable<Department> {
  return this.httpClient.get<Department>(this.api);
  }
  postData(department: Department): Promise<any> {

    return this.httpClient.post(this.api, JSON.stringify(department), this.httpOptions).toPromise();
  }
  putData(department: Department): Promise<any>{
    return this.httpClient.put(this.api, JSON.stringify(department), this.httpOptions).toPromise();
  }
  deleteData(id: number): Promise<any>{
  return  this.httpClient.delete(this.api).toPromise();
  }


}
