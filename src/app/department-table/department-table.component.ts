import { Component, OnInit } from '@angular/core';
import { Department } from './department';
import { DepartmentService } from './department-service';

@Component({
  selector: 'app-department-table',
  templateUrl: './department-table.component.html',
  styleUrls: ['./department-table.component.css']
})
export class DepartmentTableComponent implements OnInit {

  ItemsArray: Department[] = [];
  departmentName: string;
  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.getAll();

}
public getAll(): Department[] {
  let  x : Department[] = [];
  this.departmentService.getAllData().subscribe((res: Department[]) => {
    this.ItemsArray = res;
    x = res;
  });
  return x;
  }

  public postItem(): void {

    this.departmentService.postData(new Department(this.departmentName))
      .then((res) => {

        this.ItemsArray = this.getAll();
        console.log('res===>', res);
      }).catch(error => {
        console.log('error==>', error);
      })
  }

    public deleteItem(id : number): void {

    this.departmentService.deleteData(id).then((res) => { console.log('deleted ' + res); this.getAll(); }).catch(error => {
      console.log('error==>', error);
    });
  }
}
