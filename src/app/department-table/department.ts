export class Department{

  id: number;
  name: string;

  constructor(dep: string){
    this.name = dep;
  }
}
