import { Employee } from './employee';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  private api = 'http://statefarm-department-management.cfapps.io/api/employee';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  private httpClient: HttpClient;

  constructor( httpClient: HttpClient) {
    this.httpClient= httpClient;
  }

  getAllData(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.api);
  }
  getData(id: number): Observable<Employee> {
  return this.httpClient.get<Employee>(this.api);
  }

  postData(employee: Employee): Promise<any> {
    return this.httpClient.post(this.api, employee, this.httpOptions).toPromise();
  }
  putData(employee: Employee): void{
    this.httpClient.put(this.api, JSON.stringify(employee), this.httpOptions);
  }
  deleteData(id: number): Promise<any>{
    return this.httpClient.delete(`${this.api}/${id}`).toPromise();
  }


}
