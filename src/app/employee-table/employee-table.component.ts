import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './employee-service';
import { Employee } from './employee';

@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.css']
})
export class EmployeeTableComponent implements OnInit {
  ItemsArray: Employee[] = [];
 firstName: string ;
  lastName: string ;
  middleName: string ;
  departmentId: number;
  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.employeeService.getAllData().subscribe((res: Employee[]) => {
      this.ItemsArray = res;
  });
}
public deleteItem(id : number): void{

  this.employeeService.deleteData(id).then(( res ) => { console.log('deleted ' + res); }).catch(error =>{
    console.log('error==>', error);
  });
}

public postItem(): void{
this.employeeService.postData(new Employee(this.firstName, this.middleName, this.lastName, this.departmentId))
.then((res) => {
  console.log('res===>', res);
}).catch(error =>{
  console.log('error==>', error);
})
}


}
