export class Employee{



  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  departmentId: number;
  constructor(firstName: string, middleName: string, lastName: string, departmentId: number){
this.firstName = firstName;
this.middleName = middleName;
this.lastName = lastName;
this.departmentId = departmentId;
  }
}
