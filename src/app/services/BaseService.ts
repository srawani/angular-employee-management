
import {Department} from './department';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class BaseService<T> {

  private api = 'http://localhost:8080/api/';
  private endPoint: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {

   }

  getAllData(): Observable<T[]> {
    return this.httpClient.get<T[]>(this.api);
  }
  getData(id: number): Observable<T> {
  return this.httpClient.get<T>(this.api);
  }
  postData(t: T): void {

    this.httpClient.post(this.api, JSON.stringify(t), this.httpOptions);
  }
  putData(t: T): void{
    this.httpClient.put(this.api, JSON.stringify(t), this.httpOptions);
  }
  deleteData(id: number): void{
    this.httpClient.delete(this.api);
  }


}
